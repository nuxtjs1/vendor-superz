import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { LoginError } from '@/types/interfaces/errors'

// reusable aliases for mutations
export const AUTH_MUTATIONS = {
  ON_START: 'ON_START',
  SET_PAYLOAD: 'SET_PAYLOAD',
  LOGOUT: 'LOGOUT',
}

export const state = () => ({
  access_token: null as string | null, // JWT access token
  refresh_token: null as string | null, // JWT refresh token
  username: null as string | null, // username
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  [AUTH_MUTATIONS.ON_START](state) {
    const data = localStorage.getItem('tokens')
    if (data !== null) {
      const tokens = JSON.parse(data)
      state.access_token = tokens.access_token
      state.refresh_token = tokens.refresh_token
      state.username = tokens.username
    }
  },

  [AUTH_MUTATIONS.SET_PAYLOAD](state, { username, refresh, access = null }) {
    state.username = username
    state.access_token = access

    if (refresh) {
      state.refresh_token = refresh
    }
    localStorage.setItem('tokens', JSON.stringify(state))
  },

  [AUTH_MUTATIONS.LOGOUT](state) {
    state.username = null
    state.access_token = null
    state.refresh_token = null
    localStorage.setItem('tokens', JSON.stringify(state))
  },
}

export const actions: ActionTree<RootState, RootState> = {
  async login({ commit, dispatch }, { vendor_name, password }) {
    const loginError: LoginError = { detail: null }

    const payload = await this.$axios
      .post('/auth/login/login_vendor/', { vendor_name, password })
      .then((result) => {
        return result.data.tokens
      })
      .catch((error) => {
        loginError.detail = error.response.data.detail
      })

    if (loginError.detail) return loginError

    commit(AUTH_MUTATIONS.SET_PAYLOAD, payload)
    return loginError
  },

  async refresh({ commit, state }) {
    const { refresh_token } = state

    await this.$axios
      .post('/auth/token/refresh/', { refresh: refresh_token })
      .then((result) => {
        this.state.access_token = result.data.access
        localStorage.setItem('tokens', JSON.stringify(state))
      })
      .catch((error) => {
        this.commit(AUTH_MUTATIONS.LOGOUT)
      })
  },

  logout({ commit, state }) {
    commit(AUTH_MUTATIONS.LOGOUT)
  },
}

export const getters: GetterTree<RootState, RootState> = {
  isAuthenticated: (state) => {
    return state.access_token && state.access_token !== ''
  },
}
