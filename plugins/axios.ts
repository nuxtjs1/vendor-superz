import { Plugin } from '@nuxt/types'

const axios: Plugin = ({ store, app: { $axios }, redirect }) => {
  const IGNORED_PATHS = ['/auth/login/login_vendor/', '/auth/token/refresh/']

  $axios.onRequest((config) => {
    if (store.state.auth.access_token) {
      config.headers.Authorization = 'Bearer ' + store.state.auth.access_token
    }
    return config
  })

  $axios.onError((error) => {
    return new Promise(async (resolve, reject) => {
      const isIgnored: boolean = IGNORED_PATHS.some((path) =>
        error.config.url?.includes(path)
      )
      const stausCode: number = error.response ? error.response.status : -1

      if (stausCode === 401 && !isIgnored) {
        const { data: { detail } = { detail: null } } = error.response || {}
        const { refresh_token } = store.state.auth

        // -------------------------------------------------
        if (
          (detail === 'Authentication credentials were not provided.' ||
            detail === 'User not found' ||
            detail === 'Given token not valid for any token type') &&
          refresh_token
        ) {
          if (error.config.hasOwnProperty('retryAttempts')) {
            store.dispatch('auth/logout')
            return redirect('/login')
          } else {
            const config = { retryAttempts: 1, ...error.config }
            try {
              await store.dispatch('auth/refresh')
              return resolve($axios(config))
            } catch (error) {
              store.dispatch('auth/logout')
              return redirect('/login')
            }
          }
        }
      }
      return reject(error)
    })
  })
}

export default axios
