import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { ChartDataAPI } from '@/types/interfaces/chart'

export const fetchChartData = async (
  $axios: NuxtAxiosInstance
): Promise<ChartDataAPI> => {
  const data = await $axios
    .$get('/vendor_panel/charts/daily/')
    .then((response) => {
      console.log(response.data)
      return response
    })
    .catch((error) => console.log(error.response.data))
  return data as ChartDataAPI
}
