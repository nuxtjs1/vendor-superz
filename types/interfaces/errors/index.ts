export interface LoginError {
    detail: string | null
}