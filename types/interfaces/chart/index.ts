export interface Data {
  data: number[]
  label: string
  backgroundColor?: string
  borderColor?: string
  pointBackgroundColor?: string
  pointRadius?: number
  pointHoverRadius?: number
  pointHoverBorderColor?: string
  borderWidth?: number
  fill?: boolean
}

export interface ChartData {
  labels: string[]
  datasets: Data[]
}

interface Tooltips {
  backgroundColor?: string
  titleFontColor?: string
  bodyFontColor?: string
  bodySpacing?: number
  position?: string
  mode?: string
  intersect?: boolean
  xPadding?: number
  yPadding?: number
}
export interface ChartOptions {
  maintainAspectRatio?: boolean
  responsive?: boolean
  tooltips?: Tooltips
}

export interface ChartDataAPI {
  data: number[]
  labels: string[]
}
